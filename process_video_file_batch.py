import boto3
import json
import os
import uuid
from boto3.dynamodb.conditions import Key

s3 = boto3.client('s3')
s3_resource = boto3.resource('s3')
dynamodb = boto3.resource('dynamodb')

session = boto3.session.Session()
batch_client = session.client('batch')

def handler(event, context):

    print("***** this is the full event *****")
    print(event)

    jobqueue = 'Dev1' #os.environ['JobQueue']
    jobdef = 'ProcessVodDev1' #os.environ['JobDefinition']
    job1Name = 'job' + '-' + str(uuid.uuid1())

    obj = s3.get_object(Bucket=event['Records'][0]['s3']['bucket']['name'],
                        Key=event['Records'][0]['s3']['object']['key'])

    job1 = batch_client.submit_job(
        jobName=job1Name,
        jobQueue=jobqueue,
        jobDefinition=jobdef,
        containerOverrides={
            'environment': [
                {
                    'name': 'bucket_name',
                    'value': event['Records'][0]['s3']['bucket']['name'],
                },
                {
                    'name': 'bucket_key',
                    'value': event['Records'][0]['s3']['object']['key'],
                },
                {
                    'name': 'file_name',
                    'value': os.path.basename(event['Records'][0]['s3']['object']['key']),
                },
                {
                    'name': 'video_id',
                    'value': obj['Metadata']['video-id'],
                },
                {
                    'name': 'DYNAMODB_TABLE',
                    'value': os.environ['DYNAMODB_TABLE']
                },
                {
                    'name': 'cloudfront_address',
                    'value': os.environ['CLOUDFRONT_ADDRESS']
                },
                {
                    'name': 'delivery_bucket_name',
                    'value': os.environ['TARGET_VOD_BUCKET']
                },
            ]
        }
    )

    response = {
        "statusCode": 200,
        "body": json.dumps({})
    }

    return response
