import json
import time
import logging
import os

import decimalencoder
import boto3
dynamodb = boto3.resource('dynamodb')


def handler(event, context):

    data = json.loads(event['body'])

    if 'title' not in data:
        logging.error("Validation Failed")
        raise Exception("Couldn't update the item.")
        return

    timestamp = int(time.time() * 1000)

    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])

    # update the todo in the database
    result = table.update_item(
        Key={
            'id': event['pathParameters']['id']
        },
        UpdateExpression='SET title = :title, description = :description, updatedAt = :updatedAt',
        ExpressionAttributeValues={
            ':title': data['title'],
            ':description': data['description'],
            ':updatedAt': timestamp,
        },
        ReturnValues='ALL_NEW',
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(result['Attributes'],
                           cls=decimalencoder.DecimalEncoder)
    }

    return response
