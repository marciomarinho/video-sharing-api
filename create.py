import json
import logging
import os
import time
import uuid
from datetime import datetime

import boto3
dynamodb = boto3.resource('dynamodb')


def handler(event, context):

    data = json.loads(event['body'])
    if 'title' not in data:
        logging.error("Validation Failed")
        raise Exception("Couldn't create the item.")

    timestamp = str(datetime.utcnow().timestamp())

    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])

    item = {
        'id': str(uuid.uuid4()),
        'title': data['title'],
        'description': data['description']
    }

    table_response = table.put_item(Item=item)

    # create a response
    response = {
        "statusCode": 200,
        "body": json.dumps(table_response)
    }

    return response
