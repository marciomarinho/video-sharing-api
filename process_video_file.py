import json
import logging
import os
import time
import uuid
from datetime import datetime
import boto3
from shutil import copyfile

dynamodb = boto3.resource('dynamodb')
s3 = boto3.client('s3')

def handler(event, context):

    print(event)
    
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    object_key = event['Records'][0]['s3']['object']['key']
    local_file_name = '/tmp/' + object_key

    s3.download_file(bucket_name, object_key, local_file_name)
    
    dynamic_folder_name = str(uuid.uuid4())

    target_folder_name = '/tmp/' + dynamic_folder_name
    
    os.mkdir(target_folder_name)

    ffmpeg_with_args = """
        ffmpeg -hide_banner -y -i {original_file} \
        -vf scale=w=640:h=360:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename {output_folder}/360p_%03d.ts {output_folder}/360p.m3u8 \
        -vf scale=w=842:h=480:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename {output_folder}/480p_%03d.ts {output_folder}/480p.m3u8 \
        -vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename {output_folder}/720p_%03d.ts {output_folder}/720p.m3u8 \
        -vf scale=w=1920:h=1080:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename {output_folder}/1080p_%03d.ts {output_folder}/1080p.m3u8
    """.format(original_file=local_file_name, output_folder=target_folder_name)

    print('##### vai converter')
    stream = os.popen(ffmpeg_with_args)
    output = stream.read()
    print(output)
    print('##### converteu')

    copyfile('playlist.m3u8', target_folder_name + '/' + 'playlist.m3u8')

    #upload local folder to s3 vod 
    print('***** GOING TO SYNC S3 BUCKET *****')
    stream = os.popen('/opt/awscli/aws s3 sync ' + target_folder_name + ' s3://' + os.environ['TARGET_VOD_BUCKET'] + '/' + dynamic_folder_name)
    output = stream.read()
    print(output)
    print('##### S3 SYNC WITH SUCCESS #####')

    response = {
        "statusCode": 200,
        "body": json.dumps({})
    }

    return response
